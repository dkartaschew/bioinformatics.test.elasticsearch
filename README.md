ElasticSearch Test client for Bioinformatics
============================================
----
Test bed client for the kmer plugin for ElasticSearch.
Performs performance analysis of a cluster setup using defined 


Install
-------------

* run **$ mvn clean package** to build.
* java -jar target/TestESPackage-1.0.0-jar-with-dependencies.jar <sourcefolder>

Running
-------------
The application uses the ElasticSearch Java API for communicating with the local ElasticSearch Instance. (The cluster configuration should support auto-configuration).

The application will load all GenBank files from the specified folder and run a set of queries against ElasticSearch to determine performance characteristics.
