/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.application;

/**
 * Result container class.
 *
 * @author Darran Kartaschew
 */
public class Result<V> {

    /**
     * The description for this result.
     */
    private final String description;
    /**
     * The result expressed as a Long.
     */
    private final V value;

    /**
     * Create a result entry with a Long result value.
     *
     * @param name The descriptive name of the result.
     * @param value The value of the result.
     */
    public Result(String name, V value) {
        this.description = name;
        this.value = value;
    }

    /**
     * Get the description of the result.
     *
     * @return The description of the result.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the value that this result describes.
     *
     * @return the value of the result.
     */
    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return description + " : " + value.toString();
    }
}
