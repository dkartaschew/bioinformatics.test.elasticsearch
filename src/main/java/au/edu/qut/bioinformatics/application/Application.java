/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.application;

import au.edu.qut.bioinformatics.queries.BasicQueries;
import au.edu.qut.bioinformatics.queries.FileUpload;
import au.edu.qut.bioinformatics.queries.SpanQueries;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;
import au.edu.qut.bioinformatics.queries.AbstractQueries;
import au.edu.qut.bioinformatics.queries.BuildFuzzyMatrix;
import au.edu.qut.bioinformatics.queries.BuildMatrix;
import au.edu.qut.bioinformatics.queries.TestSequence;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ElasticSearch Test application.
 *
 */
public class Application {

    /**
     * Application header.
     */
    private final static String ApplicationHeader =
            "Test application for ES v1.0.0\n"
            + "------------------------------\n\n"
            + "Copyright 2013, Darran Kartaschew,\n"
            + "Apache License, Version 2.0\n\n"
            + "Package for testing ES and related activities.\n";
    /**
     * Application Help.
     */
    private final static String ApplicationHelp =
            "Usage: java -jar TestESPackage <sourcePath> <skipUpload>\n"
            + "       <sourcePath>  - The location of the files to be utilised.\n"
            + "       <skipUpload>  - Skip uploading of files before testing. (optional)\n"
            + "                       Valid options are: T, F, Y, N, 1, 0, otherwise ignored";
    /**
     * The file path to store the generated files.
     */
    private String filePath;
    /**
     * Flag to skip uploads of files
     */
    private Boolean skipUploads = false;

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public static void main(String[] args) {
        new Application().run(args);
    }

    /**
     * Main entry point.
     *
     * @param args CLI arguments.
     */
    public void run(String[] args) {
        printHeader();
        try {
            parseCLI(args);
        } catch (ApplicationException ex) {
            printHelp(ex.getMessage());
            System.exit(0);
        }
        printParameters();

        // Get our connection to the localised ElasticSearch Cluster using Zen auto-discovery.
        ElasticSearchBioInformatics instance = ElasticSearchBioInformatics.getInstance();

        try {
            // Obtain a testing sequence.
            TestSequence testSequence = new TestSequence(filePath);

            // Build a list of queries to run.
            List<AbstractQueries> queries = new ArrayList<>();

            // Upload the files. (only if the argument has been passed).
            if (!skipUploads) {
                queries.add(new FileUpload(instance, filePath));
            }
            // Basic queries (AND, SHOULD)
            queries.add(new BasicQueries(instance, testSequence, false));
            queries.add(new BasicQueries(instance, testSequence, true));
            
            // Span queries.
            queries.add(new SpanQueries(instance, testSequence, false));
            queries.add(new SpanQueries(instance, testSequence, true));
            
            // Matrix generation. (disabled)
            queries.add(new BuildMatrix(instance, filePath));
            // Fuzzy matirx generation. (disabled)
            //queries.add(new BuildFuzzyMatrix(instance, filePath));

            // Run our queries.
            for (AbstractQueries query : queries) {
                query.run();
                printResults(query.getResults());
            }

        } catch (Exception ex) {
            /*
             * Use generation Exception here instead of specific Exceptions to ensure all transport
             * and internal exceptions for ES are caught here. (To list all exceptions would
             * be a very, very long list. (Any exception caught here is fatal, so exit).
             */
            System.out.println(ex.getMessage());
            System.exit(0);
        } finally {
            // Close the connection to ES
            instance.close();
        }
    }

    /**
     * Print application header information.
     */
    private void printHeader() {
        System.out.println(ApplicationHeader);
    }

    /**
     * Print Help to console for application usage.
     */
    private void printHelp(String message) {
        System.out.println("Warning: " + message);
        System.out.println(ApplicationHelp);
    }

    /**
     * Attempts to parse the command line for runtime parameters.
     *
     * @param args command line arguments
     * @throws Exception If the parsing fails in any manner.
     */
    private void parseCLI(String[] args) throws ApplicationException {
        if (args.length < 1) {
            throw new ApplicationException("Missing command line arguments");
        }
        filePath = args[0];
        // Check the path exists and is a directory.
        File path = new File(filePath);
        if (!path.exists() || !path.isDirectory()) {
            throw new ApplicationException("Path specified appears to not exist or is invalid.");
        }
        filePath = path.getAbsolutePath();

        // check for skip uploads flag.
        if (args.length >= 2) {
            switch (args[1]) {
                case "Y":
                case "T":
                case "1":
                    skipUploads = true;
                    break;
                case "N":
                case "F":
                case "0":
                    skipUploads = false;
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Print all parameters as parsed to the command line.
     */
    private void printParameters() {
        System.out.println("Parameters:");
        System.out.println(" Path:         " + filePath);
        System.out.println(" Skip Uploads: " + skipUploads);
    }

    /**
     * Print the results to std out.
     *
     * @param results An iterator to the results set to be printed.
     */
    private void printResults(Iterator<Result> results) {
        while (results.hasNext()) {
            System.out.println(results.next());
        }
        System.out.println();
    }
}
