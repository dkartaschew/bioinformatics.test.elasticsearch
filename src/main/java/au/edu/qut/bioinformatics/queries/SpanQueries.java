/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.queries;

import au.edu.qut.bioinformatics.application.ApplicationException;
import au.edu.qut.bioinformatics.application.Result;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.elasticsearch.action.search.SearchResponse;

/**
 * Perform a basic set of span queries against ElasticSearch.
 *
 * @author Darran Kartaschew
 */
public class SpanQueries extends AbstractQueries {

    /**
     * The number of tests to run per depth.
     */
    private final int NUMBER_OF_TESTS = 3;
    /**
     * The number of times each individual test is to run.
     */
    private final int NUMBER_OF_TEST_TIMES = 3;
    /**
     * The maximum complexity of the tests sequence.
     */
    private final int MAX_TEST_DEPTH = 25;
    /**
     * The filename to log the times for uploading files.
     */
    private final String LOG_FILENAME = "spanQueries.csv";
    /**
     * Get the term vectors as part of the query.
     */
    private final boolean termsVectors;

    /**
     * Create a basic span query set.
     *
     * @param elasticSearch The connection to ElasticSearch.
     * @param sequence The test sequence to be made available to queries.
     * @throws ApplicationException The connection to ElasticSearch is NULL or invalid.
     */
    public SpanQueries(ElasticSearchBioInformatics elasticSearch, TestSequence sequence, boolean retrieveTermVectors) throws ApplicationException {
        super(elasticSearch, sequence);
        this.termsVectors = retrieveTermVectors;
    }

    /**
     * Run the upload process.
     */
    @Override
    public void run() {
        System.out.println("Starting span query tests");
        Random rnd = new Random(RANDOM_SEED);
        addResult(new Result("\nSpan Query Tests", NUMBER_OF_TESTS));

        // Run a set of tests starting from a single term up to the max depth length.
        for (int depth = 1; depth <= MAX_TEST_DEPTH; depth++) {
            addResult(new Result("Span Query Tests of depth", depth));

            // Run the number of tests per term count.
            for (int testNumber = 0; testNumber < NUMBER_OF_TESTS; testNumber++) {

                // Construct the span search based on the depth
                String term = "";
                for (int i = 0; i < depth; i++) {
                    if (i > 0) {
                        term += ",";
                    }
                    term += buildQueryTerm(rnd);
                }
                long time = 0;
                // run each query a number of times and take it's average
                for (int testIteration = 0; testIteration < NUMBER_OF_TEST_TIMES; testIteration++) {
                    SearchResponse response;
                    try {
                        response = elasticSearch.searchSpan(term, false, termsVectors);
                        System.out.print("." + response.getHits().getTotalHits());
                        time += response.getTookInMillis();
                        if (response.getHits().getTotalHits() == 0) {
                            System.out.print("-");
                        }
                    } catch (ApplicationException ex) {
                        Logger.getLogger(SpanQueries.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                time = time / NUMBER_OF_TEST_TIMES;
                addTime(time);
            }
            // Determine min/max/avg times for sending files.
            addTimesToResult();
            try {
                dumpTimesToFile(LOG_FILENAME, String.format("Depth: %d", depth));
            } catch (IOException ex) {
                Logger.getLogger(FileUpload.class.getName()).log(Level.SEVERE, null, ex);
            }
            clearTimes();
        }
        System.out.println();
    }

    /**
     * Build a string of terms to use.
     *
     * @param termCount The number of terms to use.
     * @return A string with the terms.
     */
    private String buildQueryTerm(Random rnd) {
        StringBuilder sb = new StringBuilder();

        // term 1
        int length1 = nextLength(rnd);
        int start1 = rnd.nextInt(seq.getSequenceLength() - length1);
        sb.append(seq.kmer(start1, length1));
        sb.append(" ");

        // term 2
        int length2 = nextLength(rnd);
        int start2 = rnd.nextInt(seq.getSequenceLength() - length2);
        sb.append(seq.kmer(start2, length2));
        sb.append(" ");

        // distance
        sb.append(String.format("%d", Math.abs(start2 - start1) * (MAX_KMER_LENGTH - MIN_KMER_LENGTH + 2)));
        //System.out.println(sb.toString());
        return sb.toString();
    }
}
