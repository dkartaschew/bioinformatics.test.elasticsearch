/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.queries;

import au.edu.qut.bioinformatics.application.ApplicationException;
import au.edu.qut.bioinformatics.application.Result;
import au.edu.qut.bioinformatics.dnasequence.DNASequence;
import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHitField;

/**
 * Upload all files in a specified folder into ElasticSearch.
 *
 * @author Darran Kartaschew
 */
public class BuildFuzzyMatrix extends AbstractQueries {

    /**
     * The path of the files to upload to ElasticSearch.
     */
    private final String path;
    /**
     * The filename to log the times for uploading files.
     */
    private final String LOG_FILENAME = "fmatrixLog.csv";
    /**
     * The filename to log the times for uploading files.
     */
    private final String MATRIX_FILENAME = "fmatrix.csv";
    /**
     * The filename to log the times for uploading files.
     */
    private final String RAW_MATRIX_FILENAME = "fmatrix_raw.log";
    /**
     * List of all sequences that represent our files.
     */
    private final List<String> sequences = new ArrayList<>();
    /**
     * The actual matrix.
     */
    private float[][] matrix;

    /**
     * Upload all files in the specified path to ElasticSearch.
     *
     * @param elasticSearch The connection to ElasticSearch.
     * @param path The path to the files to upload.
     * @throws ApplicationException The connection to ElasticSearch is NULL or
     * invalid.
     */
    public BuildFuzzyMatrix(ElasticSearchBioInformatics elasticSearch, String path) throws ApplicationException {
        super(elasticSearch, null);
        this.path = path;
    }

    /**
     * Run the upload process.
     */
    @Override
    public void run() {
        System.out.println("Starting fuzzy matrix build");
        File folder = new File(path);

        // Get file list.
        List<File> files = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.getName().toLowerCase().endsWith(".gbk")) {
                files.add(fileEntry);
            }
        }

        // Report an error if no files found.
        if (files.isEmpty()) {
            System.err.println("No files found in the source path");
            return;
        }

        // Read in all our files, so we have a name reference list of sequences.
        for (final File file : files) {
            try {
                DNASequence sequence = new DNASequence(file);
                sequences.add(sequence.getOrganismName()); // We assume each organism name is unique.
                /*
                 * We don't cache the sequence to preserve memory, eg 4000 sequences * 2.5MB each = 10GB
                 */
            } catch (FileNotFoundException | DNASequenceException ex) {
                Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Collections.sort(sequences);
        // Create the matrix
        matrix = new float[sequences.size()][sequences.size()];

        // Dump raw output.
        File rawfile = new File(RAW_MATRIX_FILENAME);
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(rawfile, true));
        } catch (IOException ex) {
            Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }


        // Build the matrix
        for (final File file : files) {
            try {
                DNASequence sequence = new DNASequence(file);
                int sourceIndex = sequences.indexOf(sequence.getOrganismName());
                if (out != null) {
                    try {
                        out.write(String.format("%d : %s\n", sourceIndex, sequence.getOrganismName()));
                    } catch (IOException ex) {
                        Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                SearchResponse result = elasticSearch.searchSimilarFuzzy(new String(sequence.getDNASequence()), true, false);
                addTime(result.getTookInMillis());
                // Now fill in the matrix.
                Iterator<SearchHit> iter = result.getHits().iterator();
                SearchHit lastHit = null;
                while (iter.hasNext()) {
                    SearchHit hit = iter.next();
                    if (hit != lastHit) {
                        lastHit = hit;
                        Map<String, SearchHitField> document = hit.getFields();
                        String title = (String) document.get(ElasticSearchBioInformatics.FIELD_ORGANISM).value();
                        int targetIndex = sequences.indexOf(title);
                        matrix[sourceIndex][targetIndex] = hit.getScore();
                        if (out != null) {
                            try {
                                out.write(String.format("\t%d : %s : %03.4f\n", targetIndex, title, hit.score()));
                            } catch (IOException ex) {
                                Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
                System.out.print(".");
            } catch (FileNotFoundException | DNASequenceException | NullPointerException ex) {
                Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (out != null) {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println();

        // Add some results information.
        addResult(new Result("Files compared", files.size()));
        // Determine min/max/avg times for sending files.
        addTimesToResult();

        try {
            dumpTimesToFile(LOG_FILENAME, "Fuzzy Matrix Line (msec)");
        } catch (IOException ex) {
            Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            // Save the matrix to disk.
            saveMatrix();
        } catch (IOException ex) {
            Logger.getLogger(BuildFuzzyMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Save the matrix to disk.
     *
     * @throws IOException If a disk IO error occurred on writing to disk.
     */
    private void saveMatrix() throws IOException {
        File file = new File(MATRIX_FILENAME);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file, true))) {
            for (int row = 0; row < matrix.length; row++) {
                // Print the organism name.
                out.write(sequences.get(row));
                out.write(',');
                // print out each column
                for (int column = 0; column < matrix.length; column++) {
                    out.write(String.format("%03.4f,", matrix[row][column]));
                }
                out.newLine();
            }
            out.newLine();
        }
    }

    /**
     * Print the resulting matrix to stdout.
     */
    public void printMatrix() {
        System.out.println("Similarity Fuzzy Matrix");
        for (int row = 0; row < matrix.length; row++) {
            // Print the organism name.
            System.out.print(sequences.get(row));
            System.out.print(',');
            // print out each column
            StringBuilder sb = new StringBuilder();
            for (int column = 0; column < matrix.length; column++) {
                sb.append(String.format("%03.4f,", matrix[row][column]));
            }
            System.out.println(sb.toString());
        }
        System.out.println();
    }
}
