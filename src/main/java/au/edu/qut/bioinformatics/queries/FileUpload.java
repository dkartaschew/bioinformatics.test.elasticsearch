/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.queries;

import au.edu.qut.bioinformatics.application.ApplicationException;
import au.edu.qut.bioinformatics.application.Result;
import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Upload all files in a specified folder into ElasticSearch.
 *
 * @author Darran Kartaschew
 */
public class FileUpload extends AbstractQueries {

    /**
     * The path of the files to upload to ElasticSearch.
     */
    private final String path;
    /**
     * The filename to log the times for uploading files.
     */
    private final String LOG_FILENAME = "fileupload.csv";

    /**
     * Upload all files in the specified path to ElasticSearch.
     *
     * @param elasticSearch The connection to ElasticSearch.
     * @param path The path to the files to upload.
     * @throws ApplicationException The connection to ElasticSearch is NULL or
     * invalid.
     */
    public FileUpload(ElasticSearchBioInformatics elasticSearch, String path) throws ApplicationException {
        super(elasticSearch, null);
        this.path = path;
    }

    /**
     * Run the upload process.
     */
    @Override
    public void run() {
        System.out.println("Starting file upload");
        File folder = new File(path);

        // Get file list.
        List<File> files = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.getName().toLowerCase().endsWith(".gbk")) {
                files.add(fileEntry);
            }
        }

        // Report an error if no files found.
        if (files.isEmpty()) {
            System.err.println("No files found in the source path");
            return;
        }

        // Send our files.
        for (final File file : files) {
            long start = 0;
            try {
                System.out.print(".");
                start = System.nanoTime();
                elasticSearch.sendFile(file);
            } catch (IOException | DNASequenceException ex) {
                Logger.getLogger(FileUpload.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                long end = System.nanoTime();
                addTime((end - start) / 1000000); // convert to msecs
            }
        }
        System.out.println();

        // Add some results information.
        addResult(new Result("Files uploaded", files.size()));
        // Determine min/max/avg times for sending files.
        addTimesToResult();
        try {
            dumpTimesToFile(LOG_FILENAME, "Upload Time (msec)");
        } catch (IOException ex) {
            Logger.getLogger(FileUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
