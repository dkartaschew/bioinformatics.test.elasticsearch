/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.queries;

import au.edu.qut.bioinformatics.dnasequence.DNASequence;
import au.edu.qut.bioinformatics.dnasequence.DNASequenceException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test sequence for extracting kmers from.<br>
 * The test sequence is derived from the first file found in the source folder.
 * This should ensure ALL queries return at least 1 hit highlighting worst case
 * scenario.
 *
 * @author Darran Kartaschew
 */
public class TestSequence {

    /**
     * The test sequence.
     */
    private final DNASequence sequence;

    /**
     * Create a test sequence based on a file as found within the target folder.
     *
     * @param filePath The path which contains at least 1 GenBank File.
     */
    public TestSequence(String filePath) throws FileNotFoundException, DNASequenceException {

        File folder = new File(filePath);

        // Get file list.
        List<File> files = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.getName().toLowerCase().endsWith(".gbk")) {
                files.add(fileEntry);
            }
        }

        // Report an error if no files found.
        if (files.isEmpty()) {
            System.err.println("No files found in the source path");
            throw new FileNotFoundException("No files found in the target path that can be used as a test sequence");
        }

        sequence = new DNASequence(files.get(0));
        System.out.println(String.format("Using [%s] as the test sequence\n", sequence));
    }

    /**
     * Get a kmer from the test sequence.
     *
     * @param start The start position of the sequence.
     * @param length The kmer length
     * @return A string representing the kmer or "" if unable to represent.
     */
    public String kmer(int start, int length) {
        if (start < 0 || length < 1 || start + length < sequence.getDNASequenceLength()) {
            try {
                return new String(sequence.getKMer(length, start));
            } catch (DNASequenceException ex) {
                Logger.getLogger(TestSequence.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    /**
     * Get the test sequence length.
     *
     * @return The length of the test sequence.
     */
    public Integer getSequenceLength() {
        return sequence.getDNASequenceLength();
    }
}
