/*
 * Copyright 2013 Darran Kartaschew.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package au.edu.qut.bioinformatics.queries;

import au.edu.qut.bioinformatics.application.ApplicationException;
import au.edu.qut.bioinformatics.application.Result;
import au.edu.qut.bioinformatics.elasticsearch.ElasticSearchBioInformatics;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Abstract class for building Query sets onto.
 *
 * @author Darran Kartaschew
 */
public abstract class AbstractQueries implements Runnable {

    /**
     * kmer source sequence.
     */
    protected final TestSequence seq;
    /**
     * The instance to use to connect to the ElasticSearch Cluster.
     */
    protected final ElasticSearchBioInformatics elasticSearch;
    /**
     * List of results.
     */
    private List<Result> results = new ArrayList<>();
    /**
     * The operation times for each recorded command.
     */
    private List<Long> operationTimes = new ArrayList<>();
    /**
     * Static random seed for the random number generator to ensure tests mostly run the same way.
     */
    protected final int RANDOM_SEED = 120;
    /**
     * The minimum length of the kmer.
     */
    protected final int MIN_KMER_LENGTH = 15;
    /**
     * The maximum length of the kmer.
     */
    protected final int MAX_KMER_LENGTH = 15;

    /**
     * Create a abstract query set.
     *
     * @param elasticSearch The connection to ElasticSearch.
     * @param sequence The test sequence to be made available to queries.
     * @throws ApplicationException The connection to ElasticSearch is NULL or invalid.
     */
    public AbstractQueries(ElasticSearchBioInformatics elasticSearch, TestSequence sequence) throws ApplicationException {
        if (elasticSearch == null) {
            throw new ApplicationException("The elasticsearch connection is NULL, unable to connect to ElasticSearch.");
        }
        this.elasticSearch = elasticSearch;
        this.seq = sequence;
    }

    /**
     * Add the result to the list of results.
     *
     * @param result The result to add to the list of results.
     */
    protected void addResult(Result result) {
        if (result != null) {
            results.add(result);
        }
    }

    /**
     * Get an iterator to results for displaying to the user.
     *
     * @return An iterator of results for displaying to the user.
     */
    public Iterator<Result> getResults() {
        return results.iterator();
    }

    /**
     * Get the number of results that are available.
     *
     * @return The number of results.
     */
    public Integer getResultSize() {
        return results.size();
    }

    /**
     * Add a operation time to the result set.
     *
     * @param time The time in msec to add.
     */
    protected void addTime(Long time) {
        if (time != null) {
            operationTimes.add(time);
        }
    }

    /**
     * Dump all time fields into a CSV file for later processing.
     *
     * @param filename The file to append the information to.
     * @param prefix The string to append to the start of the line.
     */
    protected void dumpTimesToFile(String filename, String prefix) throws IOException {
        File file = new File(filename);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file, true))) {
            if (prefix == null) {
                out.write("line");
            } else {
                out.write(prefix);
            }
            for (Long time : operationTimes) {
                out.write(',');
                out.write(time.toString());
            }
            out.newLine();
        }
    }

    /**
     * Clear all recorded times.
     */
    protected void clearTimes() {
        operationTimes.clear();
    }

    /**
     * Add time statistics to the result set.<br>
     * Times added are:
     * <ol>
     * <li>Minimum time.</li>
     * <li>Maximum time.</li>
     * <li>Average time.</li>
     * <li>Total time (in msec).</li>
     * <li>Total time.</li>
     * </ol>
     */
    protected void addTimesToResult() {
        long minimumTime = Integer.MAX_VALUE;
        long maximumTime = 0;
        long averageTime;
        long totalTime = 0;
        for (long time : operationTimes) {
            minimumTime = Math.min(time, minimumTime);
            maximumTime = Math.max(time, maximumTime);
            totalTime += time;
        }
        averageTime = totalTime / Math.max(1, operationTimes.size()); // avoid zero division if operationTimes.size() == 0;
        // Add our result information.
        addResult(new Result("Minimum Send Time (msec)", minimumTime));
        addResult(new Result("Maximum Send Time (msec)", maximumTime));
        addResult(new Result("Average Send Time (msec)", averageTime));
        addResult(new Result("Total Time (msec)", totalTime));
        addResult(new Result("Total Time", friendlyTime(totalTime)));
    }

    /**
     * Calculate a human friendly time from the time given.
     *
     * @param time The time in msec.
     * @return A string representation of the time.
     */
    protected String friendlyTime(long time) {
        long x = time / 1000;
        long seconds = x % 60;
        x /= 60;
        long minutes = x % 60;
        x /= 60;
        long hours = x % 24;
        return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, time % 1000);
    }

    /**
     * Get the length of the next kmer to use.
     *
     * @param rnd The random number generator being used.
     * @return The length of the next kmer.
     */
    protected int nextLength(Random rnd) {
        if (MAX_KMER_LENGTH - MIN_KMER_LENGTH != 0) {
            return rnd.nextInt(MAX_KMER_LENGTH - MIN_KMER_LENGTH) + MIN_KMER_LENGTH;
        } else {
            return MIN_KMER_LENGTH;
        }
    }
}
